import User from '../models/database/entity/User';
import dataSourse from '../ormconfig';

export default function (server) {
  server.auth.strategy('user', 'bearer-access-token', {
    validate: async (request, token, h) => {
      const userRepo = dataSourse.getRepository(User);
      const authUser = await userRepo.findOne({
        select: ['id', 'is_admin', 'token'],
        where: { token: token },
      });

      if (authUser) {
        return {
          isValid: true,
          credentials: { user: authUser },
          artifacts: {},
        };
      }
      return {
        isValid: false,
        credentials: {},
        artifacts: {},
      };
    },
  });
}
