import { createHash } from 'crypto';
import * as Boom from '@hapi/boom';

export const generateHash = (elenent: string) => {
  return createHash('md5').update(elenent).digest('hex');
};

export const showError = (e: any) => {
  console.log(e);
  return JSON.stringify(Boom.badRequest('Непредвиденная ошибка'));
};

export const isEmpty = (obj) => {
  for (let key in obj) {
    // если тело цикла начнет выполняться - значит в объекте есть свойства
    return false;
  }
  return true;
};
