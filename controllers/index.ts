import * as Boom from '@hapi/boom';
import UserRepository from '../models/database/repository';
import {
  ChangeUserRequest,
  CreateUserRequest,
  DeleteUserRequest,
  FindUserRequest,
} from './interfaces';

export default {
  createUser: async (request: CreateUserRequest) => {
    try {
      return await UserRepository.create(request.payload);
    } catch (error) {
      console.error(`UserRepository.create(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
  changeUser: async (request: ChangeUserRequest) => {
    try {
      return await UserRepository.change({
        payload: request.payload,
        auth: request.auth,
      });
    } catch (error) {
      console.error(`UserRepository.change(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
  findUser: async (request: FindUserRequest) => {
    try {
      return UserRepository.find(request.query);
    } catch (error) {
      console.error(`UserRepository.find(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
  deleteUser: async (request: DeleteUserRequest) => {
    try {
      return await UserRepository.delete({
        params: request.params,
        auth: request.auth,
      });
    } catch (error) {
      console.error(`UserRepository.create(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
};
