import * as Hapi from '@hapi/hapi';
import User from '../models/database/entity/User';
import {
  IUserChangeParams,
  IUserCreateParams,
  IUserDeleteParams,
  IUserFindParams,
} from '../models/database/repository/interfaces';

type Decorate<T> = Readonly<T> & Hapi.Request;

export type CreateUserRequest = Decorate<{
  payload: IUserCreateParams;
}>;

export type ChangeUserRequest = Decorate<{
  payload: IUserChangeParams;
  auth: any /*| any
    | User
    | undefined
  | Hapi.UserCredentials | undefined
  | { id: number; is_admin: boolean }*/;
}>;

export type FindUserRequest = Decorate<{
  query: IUserFindParams;
}>;

export type DeleteUserRequest = Decorate<{
  params: IUserDeleteParams;
  auth: any;
}>;
