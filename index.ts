import * as Hapi from '@hapi/hapi';
import * as Inert from '@hapi/inert';
import * as Vision from '@hapi/vision';
import * as HapiSwagger from 'hapi-swagger';
import routes from './routes';
import dataSource from './ormconfig';
import userAuth from './auth/userAuth';
import adminAuth from './auth/adminAuth';
import * as AuthBearer from 'hapi-auth-bearer-token';

(async () => {
  try {
    const server = Hapi.server({
      port: 8888,
      routes: {
        cors: {
          origin: ['*'],
          additionalHeaders: [
            'Accept',
            'Authorization',
            'Content-Type',
            'If-None-Match',
            'Accept-language',
          ],
        },
      },
    });

    // add plugins
    await server.register([Inert, Vision, AuthBearer]);
    await server.register({
      plugin: HapiSwagger,
      options: {
        info: {
          title: 'API Documentation',
          description: 'API Documentation',
        },
        jsonPath: '/documentation.json',
        documentationPath: '/documentation',
        schemes: ['http', 'https'],
        debug: true,
        /*securityDefinitions: {
          Bearer: {
            type: 'apiKey',
          },
        },*/
        securityDefinitions: {
          Bearer: {
            type: 'apiKey',
            name: 'Authorization',
            description: 'Bearer token',
            in: 'header',
          },
        },
        security: [{ Bearer: [] }],
      },
    });

    //auth strategy
    userAuth(server);
    adminAuth(server);

    // routes
    server.route(routes);

    // start
    await server.start();

    console.log(
      'Server running on %s://%s:%s',
      server.info.protocol,
      server.info.address,
      server.info.port
    );

    console.log(
      'Documentation running on %s://%s:%s/documentation',
      server.info.protocol,
      server.info.address,
      server.info.port
    );

    // data sourse
    await dataSource.initialize();
    await dataSource.runMigrations();
  } catch (error) {
    console.log(error);
  }
})();

// admin c7db1395-92b4-490d-9184-038c44114a39
// id 3

// user 2d635d9e-e22e-492e-bf4c-1b0467d8b1ed
//id 1
