import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column(/*'text'*/)
  surname: string;

  @Column()
  email: string;

  @Column()
  passw_hash: string;

  @Column()
  token: string;

  @Column({ select: false })
  is_admin: boolean;
}

export default User;
