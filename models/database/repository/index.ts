import dataSourse from '../../../ormconfig';
import User from '../entity/User';
import {
  IUserChangeParams,
  IUserCreateParams,
  IUserDeleteParams,
  IUserFindParams,
} from './interfaces';
import { v4 as uuidv4 } from 'uuid';
import { validateEmail, validatePassword } from '../../../routes/validators';
import { generateHash, isEmpty, showError } from '../../../common/helpers';
import * as Boom from '@hapi/boom';
import { UserCredentials } from '@hapi/hapi';

const UserRepository = {
  create: async (params: IUserCreateParams) => {
    const { name, surname, email, password, confirm_password } = params;

    //const conditions = dataSourse.createQueryBuilder(User, 'test');
    //console.log(conditions.getQueryAndParameters());

    if (password !== confirm_password) {
      //throw new Error('Пароли не совпадают');
      return JSON.stringify(Boom.badData('Пароли не совпадают'));
    }
    if (!validateEmail(email)) {
      return JSON.stringify(
        Boom.badData('Некорректный формат адреса электронной почты')
      );
    }
    if (!validatePassword(password)) {
      return JSON.stringify(
        Boom.badData(
          'Пароль должен быть не менее 8 символов и состоять из цифр и латинских букв обоих регистров'
        )
      );
    }

    const userRepo = dataSourse.getRepository(User);

    const existUser = await userRepo.findOne({
      select: ['id', 'email'],
      where: { email: email },
    });

    if (existUser) {
      return JSON.stringify(Boom.badData('Такой пользователь уже существует'));
    }

    const user = await userRepo.save({
      name,
      surname,
      email,
      passw_hash: generateHash(password),
      token: uuidv4(),
      is_admin: false,
    });

    const response = await userRepo.findOne({
      select: ['id', 'name', 'surname', 'email', 'token', 'is_admin'],
      where: { id: user.id },
    });

    if (!response) {
      return JSON.stringify(Boom.internal('Не удалось создать пользователя'));
    }

    return response;
  },

  change: async (params: { payload: IUserChangeParams; auth: any }) => {
    const userObj = params.payload;
    const userRepo = dataSourse.getRepository(User);

    const existUser = await userRepo.findOne({
      select: ['id', 'is_admin'],
      where: { id: userObj.id },
    });

    if (!existUser) {
      return JSON.stringify(Boom.badData('Такого пользователя не существует'));
    }

    const is_admin = params.auth.credentials.user.is_admin;
    const auth_user_id = params.auth.credentials.user.id;

    if (!is_admin && auth_user_id !== userObj.id) {
      return JSON.stringify(
        Boom.forbidden('Нет прав для редактирования пользователя')
      );
    }

    if (
      !is_admin &&
      auth_user_id === userObj.id &&
      userObj.is_admin !== undefined
    ) {
      return JSON.stringify(
        Boom.forbidden('Нет прав для редактирования роли пользователя')
      );
    }

    if (userObj.password && !userObj.confirm_password) {
      return JSON.stringify(Boom.badData('Заполните поле confirm_password'));
    }

    if (
      userObj.password &&
      userObj.confirm_password &&
      userObj.password !== userObj.confirm_password
    ) {
      return JSON.stringify(Boom.badData('Пароли не совпадают'));
    }

    if (userObj.email && !validateEmail(userObj.email)) {
      return JSON.stringify(
        Boom.badData('Некорректный формат адреса электронной почты')
      );
    }

    let saveObj = {};
    const dataRow = ['id', 'name', 'surname', 'email', 'password', 'is_admin'];

    dataRow.forEach((el) => {
      const canChangeFieldAdmin =
        userObj[el] !== undefined && el === 'is_admin' && is_admin;

      const canChangeUserInfo = userObj[el] !== undefined && el !== 'is_admin';

      if (canChangeUserInfo) {
        saveObj[el] = userObj[el];
      }
      if (canChangeFieldAdmin) {
        saveObj[el] = userObj[el];
      }
    });

    if (saveObj['password']) {
      saveObj['passw_hash'] = generateHash(saveObj['password']);
      delete saveObj['password'];
    }

    if (!isEmpty(saveObj)) {
      const changeUser = await userRepo.save(saveObj);
    }

    const response = await userRepo.findOne({
      select: ['id', 'name', 'surname', 'email', 'token', 'is_admin'],
      where: { id: userObj.id },
    });

    return response;
  },

  delete: async (params: { params: IUserDeleteParams; auth: any }) => {
    const { id } = params.params;

    const userRepo = dataSourse.getRepository(User);

    const existUser = await userRepo.findOne({
      select: ['id'],
      where: { id: id },
    });

    if (!existUser) {
      return JSON.stringify(Boom.badData('Такого пользователя не существует'));
    }

    const is_admin = params.auth.credentials.user.is_admin;
    const auth_user_id = params.auth.credentials.user.id;

    if (!is_admin && auth_user_id !== id) {
      return JSON.stringify(
        Boom.forbidden('Нет прав для удаления пользователя')
      );
    }

    console.log(params.params);

    const user = await userRepo.delete({
      id,
    });

    if (!user) {
      return JSON.stringify(Boom.internal('Не удалось удалить пользователя'));
    }
    return user;
  },

  find: async (params: IUserFindParams) => {
    const { offset, limit, name, surname } = params;

    const conditions = dataSourse
      .createQueryBuilder(User, 'test')
      .offset(offset)
      .limit(limit);

    if (name) {
      conditions.where('test.firstName ILIKE :firstName', {
        firstName: `%${name}%`,
      });
    }

    return await conditions.getMany();
  },
};

export default UserRepository;
