export interface IPagination {
  limit: number;
  offset: number;
}

interface IUser {
  id: number;
  name: string;
  surname: string;
  email: string;
  password: string;
  passw_hash: string;
  confirm_password: string;
  is_admin: boolean;
  token: string;
}

export type IUserCreateParams = Pick<
  IUser,
  'name' | 'surname' | 'email' | 'password' | 'confirm_password'
>;

export type IUserChangeParams = Pick<
  IUser,
  | 'id'
  | 'name'
  | 'surname'
  | 'email'
  | 'password'
  | 'confirm_password'
  | 'is_admin'
>;

export type IUserFindParams = Partial<Pick<IUser, 'name' | 'surname'>> &
  IPagination;

export type IUserDeleteParams = Pick<IUser, 'id'>;
