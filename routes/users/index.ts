import controllers from '../../controllers';
import * as Hapi from '@hapi/hapi';
import * as options from './options';

const routes: Hapi.ServerRoute[] = [
  {
    method: 'POST',
    path: '/users/registration',
    handler: controllers.createUser,
    options: options.createUser,
  },
  {
    method: 'POST',
    path: '/users/change',
    handler: controllers.changeUser,
    options: options.changeUser,
  },
  {
    method: 'GET',
    path: '/users',
    handler: controllers.findUser,
    options: options.findUser,
  },
  {
    method: 'GET',
    path: '/users/delete/{id}',
    handler: controllers.deleteUser,
    options: options.deleteUser,
  },
];

export default routes;
