import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const createUser: RouteOptions = {
  description: 'Регистрация пользователя',
  notes: 'Маршрут создания нового пользователя',
  tags: ['api', 'user'],
  validate: {
    payload: Joi.object({
      name: Joi.string()
        .required()
        .max(25)
        .example('aa')
        .description('имя пользователя')
        .messages({
          'string.base': `name должно быть строкой`,
          'string.empty': `name не может быть пустым`,
          'string.max': `name должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      surname: Joi.string()
        .required()
        .min(1)
        .max(25)
        .example('aabb')
        .description('фамилия пользователя')
        .messages({
          'string.base': `surname должно быть строкой`,
          'string.empty': `surname не может быть пустым`,
          'string.max': `surname должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      email: Joi.string()
        .max(255)
        .required()
        .email()
        .example('aaa@bb.cd')
        .messages({
          'string.base': `email должно быть строкой`,
          'string.empty': `email не может быть пустым`,
          'any.required': `Заполните обязательные поля`,
        }),
      password: Joi.string()
        .max(255)
        .required()
        .min(8)
        .example('Aabcd347')
        .messages({
          'string.base': `password должно быть строкой`,
          'string.empty': `password не может быть пустым`,
          'string.min': `password должно быть длиной минимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      confirm_password: Joi.string()
        .max(255)
        .required()
        .min(8)
        .example('Aabcd347')
        .messages({
          'string.base': `password2 должно быть строкой`,
          'string.empty': `password2 не может быть пустым`,
          'string.min': `password2 должно быть длиной минимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
    }),

    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  /*plugins: {
    'hapi-swagger': {
      responses: makeResponsesDocs(
        Joi.object({
          id: Joi.number()
            .description('id записи сгенерированный postgres')
            .example(100),
          firstName: Joi.string()
            .description('имя пользователя')
            .example('Иван'),
          lastName: Joi.string()
            .description('фамилия пользователя')
            .example('Иванов'),
        })
      ),
    },
  },*/
};
