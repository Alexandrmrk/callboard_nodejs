import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const deleteUser: RouteOptions = {
  description: 'Удаление пользователя',
  notes: 'Маршрут удаления пользователя',
  tags: ['api', 'user'],
  auth: { strategy: 'user' },
  validate: {
    params: Joi.object({
      id: Joi.number()
        .example(1)
        .description('id удаляемого пользователя')
        .required()
        .messages({
          'any.required': `Заполните обязательные поля`,
        }),
    }),

    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  /*plugins: {
    'hapi-swagger': {
      responses: makeResponsesDocs(
        Joi.object({
          id: Joi.number()
            .description('id записи сгенерированный postgres')
            .example(100),
          firstName: Joi.string()
            .description('имя пользователя')
            .example('Иван'),
          lastName: Joi.string()
            .description('фамилия пользователя')
            .example('Иванов'),
        })
      ),
    },
  },*/
};
