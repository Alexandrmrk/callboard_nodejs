export const validateEmail = (email: string): boolean => {
  const reg = /^[a-zA-Z]{3}@[a-zA-Z]{2}\.[a-zA-Z]{2}$/;
  if (reg.test(email) === false) {
    return false;
  }
  return true;
};

export const validatePassword = (password: string): boolean => {
  const reg = /^[A-Za-z0-9]+$/;
  if (reg.test(password) === false) {
    return false;
  }
  return true;
};

export const validatePhone = (phone: string): boolean => {
  const reg = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
  if (reg.test(phone) === false) {
    return false;
  }
  return true;
};
